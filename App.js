import React, { useState } from 'react';
import { Keyboard, ScrollView, StyleSheet, Text, View, Pressable } from 'react-native';
import TaskInputField from './components/TaskInputField';
import TaskItem from './components/TaskItem';
import { nouns } from './helpers/nouns';
import { adjectives } from './helpers/adjectives';
import { letters } from './helpers/letters';

export default function App() {
  const [tasks, setTasks] = useState([]);
  const [isTranslated, setIsTranslated] = useState(false);

  const addTask = (task) => {
    if (task == null) return;
    setTasks([...tasks, {
      text: theFuPfunction(task),
      isComplete: false
    }]);
    Keyboard.dismiss();
  }

  const theFuPfunction = (text) => {
    let r = Math.floor(Math.random() * 9);
    let fText = text;

    let randNoun = nouns[Math.floor(Math.random() * nouns.length)];

    switch (r) {
      case 0:
        fText = text.slice(0, text.length / 2) + randNoun + text.slice((text.length / 2) * -1);
        break;
      case 1:
        fText = randNoun + text.slice(0, text.length / 2) + "-" + text.slice((text.length / 2) * -1);
        break;
      case 2:
        fText = randNoun.slice(0, randNoun.length / 2) + text + randNoun.slice((randNoun.length / 2) * -1);
        break;
      case 3:
        let randAdj = nouns[Math.floor(Math.random() * nouns.length)];
        fText = randAdj + " " + randNoun;
        break;
      case 4:
        fText = text.slice((text.length / 2) * -1) + text.slice(0, text.length / 2);
        break;
      case 5:
        let randLetter = letters[Math.floor(Math.random() * letters.length)];
        fText = text.split("").join(randLetter);
        break;
      case 6:
        fText = text.slice(0, text.length / 1.5);
        break;
      case 7:
        fText = text.slice((text.length / 1.5) * -1);
        break;
      case 8:
        let rep = text.slice(0, text.length / 2);
        fText = rep + rep + rep + rep + text.slice((text.length / 2) * -1);
        break;
    }

    return fText;
  }

  const deleteTask = (deleteIndex) => {
    setTasks(tasks.filter((value, index) => index != deleteIndex));
  }

  const completeTask = (updateIndex) => {

    // update the tasks array 

    // need to create a new array as arrays in state are immutable 
    const updatedTasks = tasks.map((task, index) => {
      if (index == updateIndex) {

        // if the task is not complete 
        if (tasks[index].isComplete == false) {
          // complete it 
          return {
            ...task,
            isComplete: true,
          };
          // if the task is complete 
        } else {
          // uncheck it
          return {
            ...task,
            isComplete: false,
          };
        }
      } else {
        return task;
      }
    });

    setTasks(updatedTasks);

  }

  const taskText = (task) => {
    return isTranslated ? task.translation : task.text;
  }

  return (
    <View style={styles.container}>
      <Text style={styles.heading}>TODO LIST</Text>
      <ScrollView style={styles.scrollView}>
        {
          tasks.map((task, index) => {
            return (
              <View key={index} style={styles.taskContainer}>
                <TaskItem
                  index={index + 1}
                  //indexInWords={toWords(index+1)}
                  task={taskText(task)}
                  // ref={refArr.current[index]}
                  deleteTask={() => deleteTask(index)}
                  completeTask={() => completeTask(index)}
                  isComplete={task.isComplete}
                />
              </View>
            );
          })
        }
      </ScrollView>

      <TaskInputField addTask={addTask} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1E1A3C',
  },
  heading: {
    color: '#fff',
    fontSize: 20,
    fontWeight: '600',
    marginTop: 30,
    marginBottom: 10,
    marginLeft: 20,
  },
  scrollView: {
    marginBottom: 70,
  },
  taskContainer: {
    marginTop: 20,
  },
});